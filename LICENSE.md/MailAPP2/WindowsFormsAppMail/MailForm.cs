﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppMail
{
    public partial class MailForm : Form
    {
        public int UserId { get; set; }

        public MailForm(int id)
        {
            InitializeComponent();
            UserId = id;

            labelTheme.Text = UserId.ToString();
        }

        private void MailForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();  
        }
    }
}
